const discord = require("discord.js");

module.exports.run = async (bot, message, args) => {
    if(!message.member.hasPermission("ADMINISTRATOR")) return message.reply("You do no have permission to start a poll!")
    message.react("👍");
    message.react("👎");
    message.react("🤷");
}

module.exports.help = {
    name: "poll"
}