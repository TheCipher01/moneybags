const discord = require("discord.js");

module.exports.run = async (bot, message, args) => {
    if(!message.member.hasPermission("ADMINISTRATOR")) return message.reply("You do not have permsision to run this command!")
    let adminChannel = message.guild.channels.find(x => x.name === "admin-only")
    
    let helpEmbed = new discord.RichEmbed()
    .setTitle("MoneyBags Help Embed")
    .setDescription("Command Guide for MoneyBags bot")
    .setColor("#fc6400")
    .setAuthor("MoneyBags")
    .setFooter("Moneybags bot by Cipher")
    .addField("!adminhelp", "Send this Embed, only sends to the `admin-only` channel.")
    .addField("!clear <x>", "Clear x amount of messages")
    .addField("!rules", "Send rules embed.")
    .addField("!say <message>", "Takes what you say and sends it as the bot in the current channel")
    .addField("!welcome", "Send the welcome embed. (In progress)");

    adminChannel.send(helpEmbed);
    message.delete().catch(O_o=>{});

}
module.exports.help = {
    name:"adminhelp"
}