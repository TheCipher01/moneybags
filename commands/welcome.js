const discord = require("discord.js");

module.exports.run = async (bot, message, args) => {
    let welcomeChannel = message.guild.channels.find(x => x.name === "info")
    if(!message.member.hasPermission("ADMINISTRATOR")) return message.reply("You do not have permsision to run this command!")
    let welcomeEmbed = new discord.RichEmbed()
    .setTitle("Welcome to the MC Money Tycoon Discord server!")
    .setDescription("This is an example embed. This is the description")
    .addField("You can add anything else in fields like this", "And it can have two lines")
    .addField("They can also be in line", "like this", true)
    .setColor("#fc6400")
    .setFooter("Moneybag bot by Cipher");

    message.delete().catch(O_o=>{});
    welcomeChannel.send(welcomeEmbed);

}

module.exports.help = {
    name: "welcome"
}