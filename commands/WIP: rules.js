const discord = require("discord.js");

module.exports.run = async (bot, message, args) => {
    let rulesEmbed = new discord.RichEmbed()
    .setTitle("Rules")
    .addField("Rule 1:", "This is just an example")
    .addField("Rule 2:", "Don't hurt the bot")
    .setColor("#fc6400")
    .setAuthor("Moneybags")
    .setFooter("Moneybags bot created by Cipher");

    message.channel.send(rulesEmbed);

}
module.exports.help = {
    name: "rules"
}